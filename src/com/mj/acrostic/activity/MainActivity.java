package com.mj.acrostic.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.acrostic.R;
import com.mj.acrostic.service.AcrosticService;
import com.mj.acrostic.util.NetUtils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 藏头诗
 * @author zhaominglei
 * @date 2015-12-3
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	private AcrosticService acrosticService = new AcrosticService();
	private boolean isExit = false;
	private TimerTask timerTask;
	private FrameLayout frameLayout; //头部标题横条
	private LinearLayout miniAdLinearLayout; //迷你广告
	private EditText wordsEdt; //内容编辑框
	private String words; //内容
	private Spinner numSpinner; //字数选择框
	private String num; //字数
	private Spinner typeSpinner; //藏的位置选择框
	private String type; //藏的位置
	private Spinner yayuntypeSpinner; //押韵方式选择框
	private String yayuntype; //押韵方式
	private Button createBtn; //一键生成
	private ProgressBar queryProgressBar; //查询进度条
	private ImageView copyView; //复制
	private TextView resultView; //结果
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}
	
	private void init() {
		frameLayout = (FrameLayout)findViewById(R.id.acrostic_framelayout);
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		wordsEdt = (EditText)findViewById(R.id.acrostic_words_edt);
		numSpinner = (Spinner)findViewById(R.id.acrostic_num_spinner);
		typeSpinner = (Spinner)findViewById(R.id.acrostic_type_spinner);
		yayuntypeSpinner = (Spinner)findViewById(R.id.acrostic_yayuntype_spinner);
		createBtn = (Button)findViewById(R.id.acrostic_create_btn);
		queryProgressBar = (ProgressBar)findViewById(R.id.acrostic_query_progressbar);
		copyView = (ImageView)findViewById(R.id.acrostic_copy);
		resultView = (TextView)findViewById(R.id.acrostic_result);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		frameLayout.setOnClickListener(this);
		createBtn.setOnClickListener(this);
		copyView.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, "32762935f1b25843bmzWYJwhM968szSRbcv7dI6zCo6IPDNjdByd6QwXqvLcqx5OuA", "木蚂蚁");
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, "32762935f1b25843bmzWYJwhM968szSRbcv7dI6zCo6IPDNjdByd6QwXqvLcqx5OuA", "木蚂蚁");
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.acrostic_create_btn:
			words = wordsEdt.getText().toString();
			if (words == null || words.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.acrostic_words_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			num = (String)numSpinner.getSelectedItem();
			if (num == null || num.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.acrostic_num_prompt, Toast.LENGTH_SHORT).show();
				return;
			}
			type = (String)typeSpinner.getSelectedItem();
			if (type == null || type.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.acrostic_type_prompt, Toast.LENGTH_SHORT).show();
				return;
			}
			yayuntype = (String)yayuntypeSpinner.getSelectedItem();
			if (yayuntype == null || yayuntype.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.acrostic_yayuntype_prompt, Toast.LENGTH_SHORT).show();
				return;
			}
			resultView.setText("");
			
			getAcrosticInfo();
			break;
		
		case R.id.acrostic_copy:
			String result = resultView.getText().toString();
			if (result != null && !result.equals("")) {
				ClipboardManager clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
				clipboardManager.setText(result);
				Toast.makeText(getApplicationContext(), R.string.acrostic_copy_success, Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.acrostic_framelayout:
			access.openWALL(MainActivity.this);
			break;
			
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;
			
		default:
			break;
		}
	}
	
	private void getAcrosticInfo() {
		if (NetUtils.isConnected(getApplicationContext())) {
			queryProgressBar.setVisibility(View.VISIBLE);
			new AcrosticInfoAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
		}		
	}

	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	public class AcrosticInfoAsyncTask extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			return acrosticService.getAcrosticInfo(words, num, type, yayuntype);
		}

		@Override
		protected void onPostExecute(String result) {
			queryProgressBar.setVisibility(View.GONE);
			if (result != null && !result.equals("")) {
				copyView.setVisibility(View.VISIBLE);
				resultView.setText(Html.fromHtml(result));
			} else {
				copyView.setVisibility(View.GONE);
				Toast.makeText(getApplicationContext(), R.string.acrostic_error1, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}

	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
